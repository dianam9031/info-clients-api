package com.banshee.infoclients.helpers;

import org.apache.commons.codec.binary.Base64;

public class NitHelper {

    public String decodeNit(String nit){
        byte[] decodedBytes = Base64.decodeBase64(nit);
        return new String(decodedBytes);
    }

    public String encodeNit(String nit){
        byte[] encodedBytes = Base64.encodeBase64(nit.getBytes());
        return new String(encodedBytes);
    }
}
