package com.banshee.infoclients.models;

import org.springframework.beans.factory.annotation.Value;

import java.time.LocalDate;

public interface CreditLimitByClient {
    @Value("#{target.clientId}")
    int getClientId();

    @Value("#{target.fullName}")
    String getFullName();

    @Value("#{target.creditLimit}")
    int getCreditLimit();

    @Value("#{target.date}")
    LocalDate getDate();

    @Value("#{target.availableCredit}")
    int getAvailableCredit();

    @Value("#{target.visitTotal}")
    int getVisitTotal();

    @Value("#{target.net}")
    int getNet();

    @Value("#{target.visitsPercentage}")
    int getVisitsPercentage();

    @Value("#{target.id}")
    int getId();

}
