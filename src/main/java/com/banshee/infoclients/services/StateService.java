package com.banshee.infoclients.services;

import com.banshee.infoclients.dao.State;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StateService extends CrudRepository<State, Integer> {

    Iterable<State>findStatesByCountryId(int id);
}
