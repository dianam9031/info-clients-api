package com.banshee.infoclients.services;

import com.banshee.infoclients.dao.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientService extends CrudRepository<Client, Integer> {

    Iterable<Client> findByFullName(String fullName);
}
