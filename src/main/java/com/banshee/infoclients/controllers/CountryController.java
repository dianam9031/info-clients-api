package com.banshee.infoclients.controllers;

import com.banshee.infoclients.dao.Country;
import com.banshee.infoclients.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
public class CountryController {

    @Autowired
    CountryService countryService;

    @PostMapping("/country")
    Country create(@RequestBody Country country) {
        return countryService.save(country);
    }

    @GetMapping("/countries")
    Iterable<Country> read() {
        return countryService.findAll();
    }
}
