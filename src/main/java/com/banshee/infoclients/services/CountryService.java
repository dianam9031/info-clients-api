package com.banshee.infoclients.services;

import com.banshee.infoclients.dao.Country;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryService extends CrudRepository<Country, Integer> {
}
