package com.banshee.infoclients.models;

import org.springframework.beans.factory.annotation.Value;

public interface VisitsByCity {
    @Value("#{target.count}")
    int getCount();
    @Value("#{target.cityName}")
    String getCityName();
}
