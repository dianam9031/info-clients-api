package com.banshee.infoclients.controllers;

import com.banshee.infoclients.dao.State;
import com.banshee.infoclients.services.StateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class StateController {
    @Autowired
    StateService stateService;

    @PostMapping("/state")
    State create(@RequestBody State state) {
        return stateService.save(state);
    }

    @GetMapping("/states")
    Iterable<State> read() {
        return stateService.findAll();
    }

    @GetMapping("/states/{id}")
    Iterable<State> statesByCountryId(@PathVariable Integer id) {
        return stateService.findStatesByCountryId(id);
    }

    @PutMapping("/state")
    State update(@RequestBody State state) {
        return stateService.save(state);
    }

    @DeleteMapping("/state/{id}")
    void delete(@PathVariable Integer id) {
        stateService.deleteById(id);
    }
}
