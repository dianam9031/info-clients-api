package com.banshee.infoclients.services;

import com.banshee.infoclients.dao.Visit;
import com.banshee.infoclients.models.CreditLimitByClient;
import com.banshee.infoclients.models.VisitsByCity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VisitService extends CrudRepository<Visit, Integer> {

    @Query("SELECT COUNT(v.id) AS count, c.name AS cityName FROM Visit v INNER JOIN Client cl ON v.clientId = cl.id INNER JOIN City c ON cl.cityId = c.id GROUP BY c.name")
    Iterable<VisitsByCity> findByCities();

    @Query("SELECT v.id AS id, c.id AS clientId, c.fullName AS fullName, c.creditLimit AS creditLimit, c.availableCredit AS availableCredit, v.date AS date, v.visitTotal AS visitTotal, v.net AS net, c.visitsPercentage AS visitsPercentage \n" +
            "FROM Visit v \n" +
            "INNER JOIN Client c ON v.clientId = c.id\n" +
            "WHERE c.id = ?1 \n" +
            "ORDER BY v.id ASC")
    Iterable<CreditLimitByClient> findByCreditLimitByClient(int id);
}
