package com.banshee.infoclients.services;

import com.banshee.infoclients.dao.SalesRep;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SalesRepService extends CrudRepository<SalesRep, Integer> {
}
