package com.banshee.infoclients.services;

import com.banshee.infoclients.dao.City;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityService extends CrudRepository<City, Integer> {

    Iterable<City>findCitiesByStateId(int id);
}
