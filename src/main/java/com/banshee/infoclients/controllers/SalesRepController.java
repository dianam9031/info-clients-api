package com.banshee.infoclients.controllers;

import com.banshee.infoclients.dao.SalesRep;
import com.banshee.infoclients.services.SalesRepService;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class SalesRepController {

    @Autowired
    SalesRepService salesRepService;

    @PostMapping("/sales-rep")
    SalesRep create(@RequestBody SalesRep salesRep) {
        String pw_hash = BCrypt.hashpw(salesRep.getPassword(), BCrypt.gensalt(5));
        salesRep.setPassword(pw_hash);
        return salesRepService.save(salesRep);
    }

    @GetMapping("/sales-reps")
    Iterable<SalesRep> read() {
        return salesRepService.findAll();
    }

    @GetMapping("/sales-rep/{id}")
    Optional<SalesRep> findById(@PathVariable Integer id) {
        return  salesRepService.findById(id);
    }

    @PutMapping("/sales-rep")
    SalesRep update(@RequestBody SalesRep salesRep) {
        return salesRepService.save(salesRep);
    }

    @DeleteMapping("/sales-rep/{id}")
    void delete(@PathVariable Integer id) {
        salesRepService.deleteById(id);
    }
}
