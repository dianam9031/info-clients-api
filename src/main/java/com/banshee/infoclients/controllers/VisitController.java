package com.banshee.infoclients.controllers;

import com.banshee.infoclients.dao.*;
import com.banshee.infoclients.models.ClientCreditHistory;
import com.banshee.infoclients.models.CreditLimitByClient;
import com.banshee.infoclients.models.VisitsByCity;
import com.banshee.infoclients.services.ClientService;
import com.banshee.infoclients.services.SalesRepService;
import com.banshee.infoclients.services.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class VisitController {
    @Autowired
    ClientService clientService;

    @Autowired
    SalesRepService salesRepService;

    @Autowired
    VisitService visitService;

    @PostMapping("/visit")
    Visit create(@RequestBody Visit model) {
        Optional<Client> client = this.clientService.findById(model.getClientId());

        int visitTotal = model.getNet() * client.get().getVisitsPercentage();
        model.setVisitTotal(visitTotal);
        client.get().setAvailableCredit(client.get().getAvailableCredit() - visitTotal);
        return visitService.save(model);
    }

    @GetMapping("/visits")
    Iterable<Visit> read() {
        return visitService.findAll();
    }

    @GetMapping("/visits/cities")
    Iterable<VisitsByCity> findByCities(){
        return visitService.findByCities();
    }

    @GetMapping("/visits/credit-limit/{id}")
    List<ClientCreditHistory> findByCreditLimitByClient(@PathVariable Integer id){

        List<ClientCreditHistory> creditHistoy = new ArrayList<>();
        Iterable<CreditLimitByClient> response = visitService.findByCreditLimitByClient(id);
        Optional<Client> client = clientService.findById(id);
        final int[] availableCredit = {client.get().getCreditLimit()};
        response.forEach(report -> {
            availableCredit[0] = (availableCredit[0] - (report.getNet() * report.getVisitsPercentage()));
            ClientCreditHistory clientCreditHistory = new ClientCreditHistory();
            clientCreditHistory.setAvailableCredit(availableCredit[0]);
            clientCreditHistory.setCreditLimit(report.getCreditLimit());
            clientCreditHistory.setVisitTotal(report.getVisitTotal());
            clientCreditHistory.setVisitsPercentage(report.getVisitsPercentage());
            clientCreditHistory.setNet(report.getNet());
            clientCreditHistory.setDate(report.getDate());
            clientCreditHistory.setId(report.getId());
            creditHistoy.add(clientCreditHistory);


        });
        return creditHistoy;
    }

    @PutMapping("/visit")
    Visit update(@RequestBody Visit visit) {
        return visitService.save(visit);
    }

    @DeleteMapping("/visit/{id}")
    void delete(@PathVariable Integer id) {
        visitService.deleteById(id);
    }
}
