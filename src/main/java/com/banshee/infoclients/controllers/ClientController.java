package com.banshee.infoclients.controllers;

import com.banshee.infoclients.helpers.NitHelper;
import com.banshee.infoclients.dao.Client;
import com.banshee.infoclients.services.ClientService;
import com.banshee.infoclients.utils.FieldErrorMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class ClientController {

    @Autowired
    ClientService clientService;
    NitHelper nitHelper = new NitHelper();

    @PostMapping("/client")
    Client create(@Valid @RequestBody Client client) {
        String nit = nitHelper.encodeNit(client.getNit());
        client.setNit(nit);
        client.setAvailableCredit(client.getCreditLimit());
        return clientService.save(client);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    List<FieldErrorMessage> exceptionHandler(MethodArgumentNotValidException e) {
        List<FieldError> fieldErrors = e.getBindingResult().getFieldErrors();
        List<FieldErrorMessage> fieldErrorMessages = fieldErrors.stream().map(fieldError -> new FieldErrorMessage(fieldError.getField(), fieldError.getDefaultMessage())).collect(Collectors.toList());

        return fieldErrorMessages;
    }

    @GetMapping("/clients")
    Iterable<Client> read() {
        List<Client> response = new ArrayList<>();
        clientService.findAll().forEach(response::add);
        response.forEach(client -> client.setNit(nitHelper.decodeNit(client.getNit())));
        return response;
    }

    @GetMapping("/client/{id}")
    Optional<Client> findById(@PathVariable Integer id) {
        Optional<Client> client = clientService.findById(id);
        String nit = nitHelper.decodeNit(client.get().getNit());
        client.get().setNit(nit);
        return  client;
    }

    @GetMapping("/client/name")
    Iterable<Client> findByFullName(@RequestParam("fullName") String fullName){
        return clientService.findByFullName(fullName);
    }

    @PutMapping("/client")
    ResponseEntity<Client> update(@RequestBody Client client) {
        if(clientService.findById(client.getId()).isPresent())
            return new ResponseEntity(clientService.save(client), HttpStatus.OK);
        else
            return new ResponseEntity(client, HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/client/{id}")
    ResponseEntity<Client> delete(@PathVariable Integer id) {
        Optional<Client> client = clientService.findById(id);
        if(client.isPresent()) {
            clientService.deleteById(id);
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity("Client doesn't exits", HttpStatus.BAD_REQUEST);
        }

    }
}
