package com.banshee.infoclients.models;

import com.banshee.infoclients.dao.Client;

import java.time.LocalDate;

public class ClientCreditHistory {
    int availableCredit;
    int creditLimit;
    int visitTotal;
    LocalDate date;
    int visitsPercentage;
    int net;
    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNet() {
        return net;
    }

    public void setNet(int net) {
        this.net = net;
    }

    public int getVisitsPercentage() {
        return visitsPercentage;
    }

    public void setVisitsPercentage(int visitsPercentage) {
        this.visitsPercentage = visitsPercentage;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getAvailableCredit() {
        return availableCredit;
    }

    public void setAvailableCredit(int availableCredit) {
        this.availableCredit = availableCredit;
    }

    public int getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(int creditLimit) {
        this.creditLimit = creditLimit;
    }

    public int getVisitTotal() {
        return visitTotal;
    }

    public void setVisitTotal(int visitTotal) {
        this.visitTotal = visitTotal;
    }

}
