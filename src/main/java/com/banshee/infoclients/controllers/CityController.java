package com.banshee.infoclients.controllers;

import com.banshee.infoclients.dao.City;
import com.banshee.infoclients.services.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class CityController {
    @Autowired
    CityService cityService;

    @PostMapping("/city")
    City create(@RequestBody City city) {
        return cityService.save(city);
    }

    @GetMapping("/cities")
    Iterable<City> read() {
        return cityService.findAll();
    }

    @GetMapping("/cities/{id}")
    Iterable<City> citiesByStateId(@PathVariable Integer id) {
        return cityService.findCitiesByStateId(id);
    }
    @PutMapping("/city")
    City update(@RequestBody City city) {
        return cityService.save(city);
    }

    @DeleteMapping("/city/{id}")
    void delete(@PathVariable Integer id) {
        cityService.deleteById(id);
    }
}
